import gi

gi.require_version("Gtk", "4.0")
from gi.repository import (
    Gtk
)

import calliope
import musicbrainzngs
import pydbus

import dataclasses
import enum
from pathlib import Path
import logging
import sys

from .listen_history import PlaylistItemModel

log = logging.getLogger(__name__)

APP_ID = "com.gitlab.samthursfield.calliope.CalliopePopup"


@Gtk.Template(resource_path="/section_bandcamp.ui")
class SectionBandcamp(Gtk.Box):
    __gtype_name__ = "SectionBandcamp"

    artist_label = Gtk.Template.Child('artist_label')
    title_label = Gtk.Template.Child('title_label')
    album_label = Gtk.Template.Child('album_label')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update_track(self, track: PlaylistItemModel):
        self.artist_label.set_text("Not yet implemented")
        self.title_label.set_text("")
        self.album_label.set_text("")
