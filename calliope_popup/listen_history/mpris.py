# Calliope Popup
#
# Copyright 2022 Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('GObject', '2.0')
from gi.repository import (
    Gio,
    GLib,
    GObject,
)

from calliope.playlist import Item as PlaylistItem
import pydbus

import dataclasses
import enum
import logging

from .model import ListenHistoryModel, PlaylistItemModel

log = logging.getLogger(__name__)


class MprisPlaybackStatus(enum.Enum):
    PLAYING = 0
    PAUSED = 1
    STOPPED = 2

    def from_dbus(status):
        if status == 'Paused':
            return MprisPlaybackStatus.PAUSED
        if status == 'Playing':
            return MprisPlaybackStatus.PLAYING
        if status == 'Stopped':
            return MprisPlaybackStatus.STOPPED
        raise ValueError(f"Unexpected PlaybackStatus: {status}")


@dataclasses.dataclass(repr=True)
class MprisTrackMetadata():
    artists : [str]
    album : str
    title : str
    art_url : str
    track_id : str

    def to_playlist_item(self) -> PlaylistItem:
        return {
            'creator': ', '.join(self.artists),
            'calliope.creator_list': self.artists,
            'album': self.album,
            'title': self.title,
            'image': self.art_url,
        }


class MprisMonitor(GObject.Object):
    """Watch MPRIS D-Bus API for local now-playing notifications.

    For more details:

      * https://wiki.archlinux.org/title/MPRIS
      * https://specifications.freedesktop.org/mpris-spec/latest/

    """
    MPRIS_NAME = "org.mpris.MediaPlayer2"
    MPRIS_OBJECT = "/org/mpris/MediaPlayer2"
    MPRIS_PLAYER_IFACE = 'org.mpris.MediaPlayer2.Player'

    def __init__(self):
        super(MprisMonitor, self).__init__()

        self.bus = None

        self._playback_status = MprisPlaybackStatus.STOPPED
        self._track_metadata = None

    @GObject.Property
    def playback_status(self) -> MprisPlaybackStatus:
        return self._playback_status

    @GObject.Property
    def track_metadata(self) -> MprisTrackMetadata:
        return self._track_metadata

    def start(self, cancellable=None):
        self.bus = pydbus.SessionBus()

        self.bus.subscribe(
            object=self.MPRIS_OBJECT,
            iface="org.freedesktop.DBus.Properties",
            signal="PropertiesChanged",
            signal_fired=self._on_mpris_properties_changed)

        player = self._get_best_player(self.bus)
        if player:
            self._update_playback_status(player.PlaybackStatus)
            self._update_track_metadata(player.Metadata)

    def _update_track_metadata(self, mpris_metadata):
        artists = list(filter(None, mpris_metadata.get('xesam:artist', [])))
        track = MprisTrackMetadata(
            track_id=mpris_metadata["mpris:trackid"],
            album=mpris_metadata.get('xesam:album', ''),
            artists=artists,
            title=mpris_metadata.get('xesam:title', ''),
            art_url=mpris_metadata.get('mpris:artUrl'),
        )

        if len(track.artists) == 0 or len(track.title) == 0:
            log.info("Discarding empty track metadata.")
        else:
            self._track_metadata = track
            self.notify('track-metadata')

    def _update_playback_status(self, status):
        self._playback_status = MprisPlaybackStatus.from_dbus(status)
        self.notify('playback-status')

    def _on_mpris_properties_changed(self, sender, obj, iface, signal, params):
        log.debug("Received: %s from %s, params: %s", sender, obj, params)
        assert obj == self.MPRIS_OBJECT
        assert signal == "PropertiesChanged"
        assert params[0] == self.MPRIS_PLAYER_IFACE

        mpris_properties = params[1]
        if "Metadata" in mpris_properties:
            self._update_track_metadata(mpris_properties["Metadata"])
        if 'PlaybackStatus' in mpris_properties:
            self._update_playback_status(mpris_properties['PlaybackStatus'])

    def _list_players(self, bus):
        dbus = self.bus.get('.DBus', '/org/freedesktop/DBus')
        names = dbus.ListNames()
        mpris_prefix = self.MPRIS_NAME + "."
        return [name for name in names if name.startswith(mpris_prefix)]

    def _get_best_player(self, bus):
        player_names = self._list_players(self.bus)
        log.debug("Got names: %s", player_names)
        best_player = None
        best_status = MprisPlaybackStatus.STOPPED
        for player_name in player_names:
            player = bus.get(player_name, self.MPRIS_OBJECT)
            status = MprisPlaybackStatus.from_dbus(player.PlaybackStatus)

            if status == MprisPlaybackStatus.STOPPED:
                log.debug("Player %s is stopped", player_name)
            elif status == MprisPlaybackStatus.PAUSED:
                log.debug("Player %s is paused", player_name)
                if best_status == MprisPlaybackStatus.STOPPED:
                    log.debug("Best player now %s", player_name)
                    best_status = status
                    best_player = player
            elif status == MprisPlaybackStatus.PLAYING:
                log.debug("Player %s is playing", player_name)
                if best_status != MprisPlaybackStatus.PLAYING:
                    log.debug("Best player now %s", player_name)
                    best_status = status
                    best_player = player
        return best_player


class MprisHistoryProvider(ListenHistoryModel, Gio.ListModel):
    """Monitor and store MPRIS track notifications."""

    def __init__(self, max_history_size=10000):
        super(MprisHistoryProvider, self).__init__()

        self.history = []
        self.max_history_size = max_history_size

        self.monitor = MprisMonitor()
        self.monitor.connect('notify::track-metadata',
                             self.on_monitor_track_metadata_changed)

    def do_get_item_type(self):
        return PlaylistItemModel.gtype

    def do_get_n_items(self) -> int:
        return len(self.history)

    def do_get_item(self, position) -> PlaylistItemModel:
        try:
            return PlaylistItemModel(
                self.history[position].to_playlist_item()
            )
        except IndexError:
            return None

    def start_monitor(self, cancellable=None):
        self.monitor.start(cancellable)

    def on_monitor_track_metadata_changed(self, monitor, pspec):
        notify_added = False
        notify_removed = False

        if self.monitor.track_metadata is not None:
            self.history.insert(0, self.monitor.track_metadata)
            notify_added = True

            if len(self.history) > self.max_history_size:
                self.history.pop()
                notify_removed = True

        if notify_added:
            def notify_new_item():
                changed_position = 0
                added_count = 1
                self.items_changed(changed_position, 0, added_count)
            GLib.idle_add(notify_new_item)

        if notify_removed:
            def notify_removed_item():
                changed_position = len(self.history)
                removed_count = 1
                self.items_changed(changed_position, removed_count, 0)
            GLib.idle_add(notify_removed_item)
