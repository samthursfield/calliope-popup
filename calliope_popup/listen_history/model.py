# Calliope Popup
# Copyright (C) 2022  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi

gi.require_version('GLib', '2.0')
from gi.repository import (
    Gio, GObject
)


class PlaylistItemModel(GObject.Object):
    def __init__(self, data):
        super(PlaylistItemModel, self).__init__()
        self.data = data

    def get_data(self):
        return self.data


class ListenHistoryModel(GObject.GObject):
    """Data model used by listen history providers.

    Subclasses must implement Gio.ListModel interface.

    """

    def start_monitor(self, cancellable=None):
        return

    def stop_monitor(self, cancellable=None):
        return
