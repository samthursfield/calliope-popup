# Calliope Popup
# Copyright (C) 2022  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi

from gi.repository import (
    Gio, GLib
)

from calliope.playlist import Playlist

from .model import ListenHistoryModel, PlaylistItemModel


class PlaylistHistoryProvider(ListenHistoryModel, Gio.ListModel):
    """A "listen history" provider which is actually a plain old playlist!"""
    def __init__(self, playlist: Playlist):
        super(PlaylistHistoryProvider, self).__init__()
        self.playlist = playlist

    def start_monitor(self):
        GLib.idle_add(
            lambda: self.items_changed(0, len(self.playlist), 0)
        )

    def do_get_item_type(self):
        return PlaylistItemModel.gtype

    def do_get_n_items(self):
        return len(self.playlist)

    def do_get_item(self, position):
        try:
            return PlaylistItemModel(self.playlist[position])
        except IndexError:
            return None
