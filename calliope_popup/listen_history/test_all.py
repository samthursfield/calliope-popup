# Calliope Popup
# Copyright (C) 2022  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Unit tests for listen_history module."""


from .playlist import PlaylistHistoryProvider


def test_playlist_history_provider():
    playlist = [
        { "creator": "The Tests",
          "title": "Test 1"
        },
        { "creator": "The Tests",
          "title": "Test 2",
          "album": "A whole album of unit tests",
        }
    ]

    provider = PlaylistHistoryProvider(playlist)
    assert provider.get_n_items() == 2

    first_item = provider.get_item(0)
    assert first_item.get_data()["title"] == "Test 1"
