import gi

gi.require_version("Adw", "1")
gi.require_version('GLib', '2.0')
gi.require_version("Gtk", "4.0")
from gi.repository import (
    Gio, Gtk,
)

import calliope.import_

import logging
from pathlib import Path
import sys

from .app_window import AppWindow
from .listen_history import MprisHistoryProvider, PlaylistHistoryProvider

log = logging.getLogger(__name__)

APP_ID = "com.gitlab.samthursfield.calliope.CalliopePopup"


class Application(Gtk.Application):
    def __init__(self, playlist=None):
        super().__init__(application_id=APP_ID)

        if playlist:
            playlist_path = Path(playlist)
            text = playlist_path.read_text()
            playlist_data = calliope.import_.import_(text)
            self.history_provider = PlaylistHistoryProvider(playlist_data)
        else:
            self.history_provider = MprisHistoryProvider()

        self.window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.window = AppWindow(
                self.history_provider,
                application=self,
                title="Main Window",
            )

        self.window.present()

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()
