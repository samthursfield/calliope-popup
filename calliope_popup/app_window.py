import gi

gi.require_version("Adw", "1")
gi.require_version('GLib', '2.0')
gi.require_version("Gtk", "4.0")
from gi.repository import (
    Adw, Gio, GLib, Gtk,
)

import logging

from . import now_playing_monitor
from .section_home import SectionHome
from .section_bandcamp import SectionBandcamp
from .section_musicbrainz import SectionMusicBrainz
from .listen_history import PlaylistItemModel

log = logging.getLogger(__name__)

@Gtk.Template(resource_path="/app.ui")
class AppWindow(Adw.ApplicationWindow):
    __gtype_name__ = "AppWindow"

    stack = Gtk.Template.Child('stack')

    def __init__(self, history_provider, application=None, title=None):
        super().__init__(application=application, title=title)

        self.sections = {}

        self.add_section(SectionHome(), 'home', 'Home')
        self.add_section(SectionBandcamp(), 'bandcamp', 'Bandcamp')
        self.add_section(SectionMusicBrainz(), 'musicbrainz', 'MusicBrainz')

        self.history_provider = history_provider
        self.history_position = 0

        action = Gio.SimpleAction.new("history-back", None)
        action.connect("activate", self.on_history_back)
        self.add_action(action)

        action = Gio.SimpleAction.new("history-forwards", None)
        action.connect("activate", self.on_history_forwards)
        self.add_action(action)

        GLib.idle_add(self.start_history_provider)

    def start_history_provider(self):
        self.history_provider.connect(
            'items-changed',
            self.on_history_provider_items_changed,
        )

        self.history_provider.start_monitor()

        self.update_track(self.history_provider.get_item(0))
        self.update_back_forwards_actions()

    def add_section(self, section, name, title):
        self.stack.add_titled(section, name, title)
        self.sections[name] = section

    def update_track(self, track: PlaylistItemModel):
        log.debug("Got track: %s", track)

        for section in self.sections.values():
            section.update_track(track)

    def update_back_forwards_actions(self):
        can_go_back = (self.history_position < self.history_provider.get_n_items() - 1)
        self.lookup_action('history-back').set_enabled(can_go_back)

        can_go_forwards = (self.history_position > 0)
        self.lookup_action('history-forwards').set_enabled(can_go_forwards)

    def on_history_provider_items_changed(
        self, history_provider, position, removed, added
    ):
        if position == self.history_position:
            self.update_track(history_provider.get_item(self.history_position))
        self.update_back_forwards_actions()

    def on_history_back(self, action, parameter):
        if self.history_position + 1 < self.history_provider.get_n_items():
            self.history_position += 1

            item = self.history_provider.get_item(self.history_position)
            self.update_track(item)
            self.update_back_forwards_actions()

    def on_history_forwards(self, action, parameter):
        if self.history_position > 0:
            self.history_position -= 1

            item = self.history_provider.get_item(self.history_position)
            self.update_track(item)
            self.update_back_forwards_actions()
