import gi

gi.require_version("Gtk", "4.0")
from gi.repository import (
    GLib,
    Gio,
    Gtk
)

import calliope
import musicbrainzngs
import pydbus

import dataclasses
import enum
from pathlib import Path
import logging
import sys
from threading import Thread

from .listen_history import PlaylistItemModel

log = logging.getLogger(__name__)

APP_ID = "com.gitlab.samthursfield.calliope.CalliopePopup"


def resolve_track_sync(track: PlaylistItemModel, section, cancellable=None):
    mb = calliope.musicbrainz.MusicbrainzContext({})
    data = track.get_data()

    result = calliope.musicbrainz.resolve.release_ids_from_album(
        mb, data
    )

    if cancellable and cancellable.is_cancelled():
        return

    if 'musicbrainz.release_id' not in result:
        log.info("No match, trying Various Artists")
        result = calliope.musicbrainz.resolve.release_ids_from_album(
            mb,
            {
                'creator': "Various Artists",
                'album': data.get('album'),
            }
        )

    if cancellable and cancellable.is_cancelled():
        return

    if 'musicbrainz.release_id' in result:
        # FIXME: go via calliope!
        release_info = musicbrainzngs.get_release_by_id(
            result['musicbrainz.release_id'],
            includes=['artists']
        )
        log.debug(release_info)
        artist_credit = ', '.join(
            artist['artist']['name']
            for artist in release_info['release']['artist-credit']
        )
        album = release_info['release']['title']
        GLib.idle_add(
            lambda: section.set_info(artist_credit, "", album))
    else:
        log.info("Didn't find track on Musicbrainz")
        GLib.idle_add(
            lambda: section.set_info("Not found", None, None))


@Gtk.Template(resource_path="/section_musicbrainz.ui")
class SectionMusicBrainz(Gtk.Box):
    __gtype_name__ = "SectionMusicBrainz"

    artist_label = Gtk.Template.Child('artist_label')
    title_label = Gtk.Template.Child('title_label')
    album_label = Gtk.Template.Child('album_label')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__thread = None
        self.__cancellable = None

    def set_info(self, artist, title, album):
        self.artist_label.set_text(artist or "")
        self.title_label.set_text(title or "")
        self.album_label.set_text(album or "")

    def update_track(self, track: PlaylistItemModel):
        self.set_info("Updating...", None, None)

        if self.__thread:
            self.__cancellable.cancel()
            self.__thread = None
            self.__cancellable = None

        if track is None:
            self.set_info("Not playing", None, None)
        else:
            self.__cancellable = Gio.Cancellable.new()
            self.__thread = Thread(
                target=resolve_track_sync,
                args=[
                    track,
                    self,
                    self.__cancellable
                ]
            )
            self.__thread.start()
