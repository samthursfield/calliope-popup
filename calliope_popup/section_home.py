import gi

gi.require_version("Gtk", "4.0")
from gi.repository import (
    Gtk
)

import calliope
import musicbrainzngs
import pydbus

import dataclasses
import enum
from pathlib import Path
import logging
import sys

from .listen_history import PlaylistItemModel

log = logging.getLogger(__name__)

APP_ID = "com.gitlab.samthursfield.calliope.CalliopePopup"


def ui_filename(filename):
    result = Path(__file__).parent.joinpath(filename)
    return str(result)


@Gtk.Template(resource_path=ui_filename("/section_home.ui"))
class SectionHome(Gtk.Box):
    __gtype_name__ = "SectionHome"

    artist_label = Gtk.Template.Child('artist_label')
    title_label = Gtk.Template.Child('title_label')
    album_label = Gtk.Template.Child('album_label')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def set_info(self, artist, title, album):
        self.artist_label.set_text(artist)
        self.title_label.set_text(title)
        self.album_label.set_text(album)

    def update_track(self, track: PlaylistItemModel):
        if track is None:
            self.set_info('No artist', 'No title', 'No album')
            return
        else:
            data = track.get_data()
            self.set_info(
                data.get('creator', "No artist"),
                data.get('title', "No title"),
                data.get('album', "No album"),
            )
